# Hippoplanner

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Requirements
HippoPlanner will be a webapplication where users can search for all horse-related events in Belgium.
- On the home page, the user sees a little text and a map of the events in the upcoming two weeks.
- The user can see the events devided in following categories: Jumping, Dressage, Eventing and other. Each categorie is devided in Flanders and Wallonia.
- The user sees following data on every event page:
  - Date;
  - Organisation;
  - Location;
  - (Sign up).
- The user will also see a page with an archive of all events in the past, this one will have following data:
  - Date;
  - Discipline;
  - Organisation;
  - Location;
  - (Result).
- The user will have the possibility to search on all fields on every page.
- The user can visit a detail page with following data:
  - Date;
  - Location;
  - ...
- The user can find a contact page with two sections:
  - A contact form; to send any question, comment, ...
  - An event form; to enter an event which the admin can accept, adjust or decline.
- The user will see HippoPlanners' partners and events in the picture (place to be defined).
- The user will find HippoPlanner easily on Google with the best possible optimalisation.

Admin
- The admin can see the same event pages as the user but he doesn't have a splitting for Flanders, Wallonia or archive.
- The admin can archive via a button that will check past dates and set it to archived.
- The admin can create events via a button on the event list page (Both jumping, dressage, eventing and other)
- The admin can edit and delete event via the detail page of the event.
- The admin can view archived events via a slider.
- When the admin sees the archived events, the admin has the possibility to filter on events without results so he can easily add them.
- The admin can see a list where he can see the results of the event form.
