import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { OrderModule } from 'ngx-order-pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { WedstrijdService } from './services/wedstrijden.service';

import { AppComponent } from './app.component';
import { PublicContainer } from './components/public/public.container';
import { HomeComponent } from './components/public/homeComponent/home.component';
import { WedstrijdComponent } from './components/public/wedstrijdComponent/wedstrijd.component';
import { WedstrijdDetailComponent } from './components/public/wedstrijdComponent/wedstrijdDetail.component';
import { ContactComponent } from './components/public/contactComponent/contact.component';
import { AdminContainer } from './components/admin/admin.container';
import { AdminWedstrijdComponent } from './components/admin/wedstrijdComponent/wedstrijd.component';
import { AdminWedstrijdDetailComponent } from './components/admin/wedstrijdComponent/wedstrijdDetail.component';
import { AdminContactComponent } from './components/admin/contactComponent/contact.component';
import { AdminLoginComponent } from './components/admin/loginComponent/login.component';
import { AuthService } from './services/auth.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { NgxPaginationModule } from 'ngx-pagination';
import { environment } from '../environments/environment';

// create routes
const appRoutes: Routes = [
  {
    path: '',
    component: PublicContainer,
    children: [
      { path: 'home', component: HomeComponent },
      {
        path: 'wedstrijd/:discipline',
        children: [
          {
            path: ':deelVanBelgie',
            component: WedstrijdComponent,
          },
          { path: ':deelVanBelgie/:id', component: WedstrijdDetailComponent },
          { path: '', redirectTo: 'overzicht', pathMatch: 'full' },
        ],
      },
      { path: 'contact', component: ContactComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
    ],
  },
  {
    path: 'admin',
    component: AdminContainer,
    children: [
      { path: 'wedstrijd/:discipline', component: AdminWedstrijdComponent },
      { path: 'wedstrijd/:discipline/details/:id', component: AdminWedstrijdDetailComponent },
      { path: 'wedstrijd/:discipline/details', component: AdminWedstrijdDetailComponent },
      { path: 'contact', component: AdminContactComponent },
      { path: '', redirectTo: 'wedstrijd/jumping', pathMatch: 'full' },
    ],
  },
  { path: 'login', component: AdminLoginComponent },
  { path: '**', redirectTo: 'home' }
  // { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    PublicContainer,
    HomeComponent,
    WedstrijdComponent,
    WedstrijdDetailComponent,
    ContactComponent,
    AdminContainer,
    AdminWedstrijdComponent,
    AdminWedstrijdDetailComponent,
    AdminContactComponent,
    AdminLoginComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDskIywWV9IlJ9t_RYYWDck6Vr6UUtTbRk',
    }),
    OrderModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    NgxPaginationModule,
    AngularFireDatabaseModule,
  ],
  providers: [WedstrijdService, AuthService],
  bootstrap: [AppComponent],
})
export class AppModule { }
