import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@Injectable()
export class AuthService {
  constructor(private fireAuth: AngularFireAuth) {}

  login(email, password) {
    return this.fireAuth.auth.signInWithEmailAndPassword(email, password);
  }
}
