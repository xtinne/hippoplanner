import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/add/operator/map';
import { Wedstrijd } from '../models/wedstrijd.model';
import { Location } from '@angular/common';

export interface IWedstrijdDTO {
  id: string;
  startdatum: Date;
  einddatum?: Date;
  organisatie: string;
  locatie: string;
  adres: string;
  gemeente: string;
  provincie: string;
  deelVanBelgie: string;
  inschrijving?: IInschrijving;
  startlijst?: IStartlijst;
  resultaten?: string;
  proeven?: string[];
  bodem?: string;
  omgeving?: string;
  fotograaf?: IFotograaf[];
  gearchiveerd: boolean;
  kaart?: string;
}

export interface IFotograaf {
  naam: number;
  website?: string;
}

export interface IInschrijving {
  text: string;
  link: string;
}

export interface IStartlijst {
  text: string;
  link: string;
}

@Injectable()
export class WedstrijdService {
  jumpings;

  constructor(private httpClient: HttpClient, private fireAuth: AngularFireAuth, private fireDb: AngularFireDatabase, private location: Location) { }

  saveWedstrijd(discipline, wedstrijd) {
    console.log(wedstrijd);
    switch (discipline) {
      case 'jumping':
        this.saveJumping(wedstrijd);
        break;
      case 'dressuur':
        this.saveDressage(wedstrijd);
        break;
      case 'eventing':
        this.saveEventing(wedstrijd);
        break;
      case 'overig':
        this.saveOverig(wedstrijd);
        break;
      default:
        break;
    }
  }

  getAllJumpings(): Observable<IWedstrijdDTO[]> {
    return this.httpClient
      .get<Map<String, IWedstrijdDTO>>(`https://hippoplanner-4e6d6.firebaseio.com/jumping.json`)
      .map(wedstrijdDTO => {
        const wedstrijden = [];
        for (const dto in wedstrijdDTO) {
          if (wedstrijdDTO.hasOwnProperty(dto)) {
            const wedstrijd = wedstrijdDTO[dto];
            wedstrijd.id = dto;
            wedstrijd.sorteerDatum = new Date(wedstrijd.startdatum).getTime();
            wedstrijden.push(new Wedstrijd(wedstrijd));
          }
        }
        return wedstrijden;
      });
  }

  getJumping(id): Observable<Wedstrijd> {
    return this.httpClient
      .get<IWedstrijdDTO>(`https://hippoplanner-4e6d6.firebaseio.com/jumping/${id}.json`)
      .map(wedstrijdDTO => new Wedstrijd(wedstrijdDTO));
  }

  saveJumping(jumping): void {
    if (jumping.id) {
      const jumpingList = this.fireDb.list('jumping');
      jumpingList.update(jumping.id, jumping).then(() => console.log('Updated'));
    } else {
      const jumpingList = this.fireDb.list('jumping');
      jumpingList.push(jumping).then(() => console.log('Created'));
    }
  }

  deleteJumping(id): void {
    const jumpingList = this.fireDb.list('jumping');
    jumpingList.remove(id).then(() => this.location.back());
  }

  getAllDressages(): Observable<IWedstrijdDTO[]> {
    return this.httpClient
      .get<Map<String, IWedstrijdDTO>>(`https://hippoplanner-4e6d6.firebaseio.com/dressuur.json`)
      .map(wedstrijdDTO => {
        const wedstrijden = [];
        for (const dto in wedstrijdDTO) {
          if (wedstrijdDTO.hasOwnProperty(dto)) {
            const wedstrijd = wedstrijdDTO[dto];
            wedstrijd.id = dto;
            wedstrijd.sorteerDatum = new Date(wedstrijd.startdatum).getTime();
            wedstrijden.push(new Wedstrijd(wedstrijd));
          }
        }
        return wedstrijden;
      });
  }

  getDressage(id): Observable<Wedstrijd> {
    return this.httpClient
      .get<IWedstrijdDTO>(`https://hippoplanner-4e6d6.firebaseio.com/dressuur/${id}.json`)
      .map(wedstrijdDTO => new Wedstrijd(wedstrijdDTO));
  }

  saveDressage(dressuur): void {
    if (dressuur.id) {
      const dressageList = this.fireDb.list('dressuur');
      dressageList.update(dressuur.id, dressuur).then(() => console.log('Updated'));
    } else {
      const dressageList = this.fireDb.list('dressuur');
      dressageList.push(dressuur).then(() => console.log('Created'));
    }
  }

  deleteDressage(id): void {
    const dressageList = this.fireDb.list('dressuur');
    dressageList.remove(id).then(() => this.location.back());
  }

  getAllEventings(): Observable<IWedstrijdDTO[]> {
    return this.httpClient
      .get<Map<String, IWedstrijdDTO>>(`https://hippoplanner-4e6d6.firebaseio.com/eventing.json`)
      .map(wedstrijdDTO => {
        const wedstrijden = [];
        for (const dto in wedstrijdDTO) {
          if (wedstrijdDTO.hasOwnProperty(dto)) {
            const wedstrijd = wedstrijdDTO[dto];
            wedstrijd.id = dto;
            wedstrijd.sorteerDatum = new Date(wedstrijd.startdatum).getTime();
            wedstrijden.push(new Wedstrijd(wedstrijd));
          }
        }
        return wedstrijden;
      });
  }

  getEventing(id): Observable<Wedstrijd> {
    return this.httpClient
      .get<IWedstrijdDTO>(`https://hippoplanner-4e6d6.firebaseio.com/eventing/${id}.json`)
      .map(wedstrijdDTO => new Wedstrijd(wedstrijdDTO));
  }

  saveEventing(eventing): void {
    if (eventing.id) {
      const eventingList = this.fireDb.list('eventing');
      eventingList.update(eventing.id, eventing).then(() => console.log('Updated'));
    } else {
      const eventingList = this.fireDb.list('eventing');
      eventingList.push(eventing).then(() => console.log('Created'));
    }
  }

  deleteEventing(id): void {
    const eventingList = this.fireDb.list('eventing');
    eventingList.remove(id).then(() => this.location.back());
  }

  getAllOverige(): Observable<IWedstrijdDTO[]> {
    return this.httpClient
      .get<Map<String, IWedstrijdDTO>>(`https://hippoplanner-4e6d6.firebaseio.com/overig.json`)
      .map(wedstrijdDTO => {
        const wedstrijden = [];
        for (const dto in wedstrijdDTO) {
          if (wedstrijdDTO.hasOwnProperty(dto)) {
            const wedstrijd = wedstrijdDTO[dto];
            wedstrijd.id = dto;
            wedstrijd.sorteerDatum = new Date(wedstrijd.startdatum).getTime();
            wedstrijden.push(new Wedstrijd(wedstrijd));
          }
        }
        return wedstrijden;
      });
  }

  getOverig(id): Observable<Wedstrijd> {
    return this.httpClient
      .get<IWedstrijdDTO>(`https://hippoplanner-4e6d6.firebaseio.com/overig/${id}.json`)
      .map(wedstrijdDTO => new Wedstrijd(wedstrijdDTO));
  }

  saveOverig(overig): void {
    if (overig.id) {
      const overigList = this.fireDb.list('overig');
      overigList.update(overig.id, overig).then(() => console.log('Updated'));
    } else {
      const overigList = this.fireDb.list('overig');
      overigList.push(overig).then(() => console.log('Created'));
    }
  }

  deleteOverig(id): void {
    const overigList = this.fireDb.list('overig');
    overigList.remove(id).then(() => this.location.back());
  }
}
