import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.container.html',
})
// tslint:disable-next-line:component-class-suffix
export class AdminContainer implements OnInit {
  authorized;

  constructor(public fireAuth: AngularFireAuth, private router: Router) {}

  ngOnInit() {
    this.fireAuth.authState.subscribe(auth => {
      if (!auth) {
        if (this.authorized) {
          this.router.navigate([`/`]);
        } else {
          this.router.navigate([`/login`]);
        }
      }
      this.authorized = !!auth;
    });
  }

  logOut() {
    this.fireAuth.auth.signOut();
  }
}
