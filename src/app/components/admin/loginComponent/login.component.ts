import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './login.component.html',
})
export class AdminLoginComponent implements OnInit {
  form;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    this.authService
      .login(this.form.value.email, this.form.value.password)
      .then(success => {
        console.log(success);
        this.router.navigate([`/admin`]);
      });
  }
}
