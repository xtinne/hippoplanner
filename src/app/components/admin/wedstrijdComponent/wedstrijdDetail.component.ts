import { Component, OnInit } from '@angular/core';
import { WedstrijdService } from '../../../services/wedstrijden.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Wedstrijd } from '../../../models/wedstrijd.model';

@Component({
  selector: 'app-admin-dressuur-detail',
  templateUrl: './wedstrijdDetail.component.html',
})
export class AdminWedstrijdDetailComponent implements OnInit {
  wedstrijdId;
  wedstrijd: Wedstrijd = new Wedstrijd();
  discipline;
  form: FormGroup;

  constructor(
    private wedstrijdService: WedstrijdService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      startdatum: new FormControl('', [Validators.required]),
      einddatum: new FormControl(),
      organisatie: new FormControl('', [Validators.required]),
      locatie: new FormControl(),
      adres: new FormControl(),
      gemeente: new FormControl('', [Validators.required]),
      provincie: new FormControl('', [Validators.required]),
      deelVanBelgie: new FormControl('', [Validators.required]),
      inschrijving: new FormGroup({
        text: new FormControl(),
        link: new FormControl(),
      }),
      startlijst: new FormGroup({
        text: new FormControl(),
        link: new FormControl(),
      }),
      resultaten: new FormControl(),
      proeven: new FormControl(),
      bodem: new FormControl(),
      kaart: new FormControl(),
      omgeving: new FormControl(),
      fotografen: new FormGroup({
        naam: new FormControl(),
        website: new FormControl(),
      }),
      informatie: new FormControl(),
      type: new FormControl(),
      extra: new FormControl(),
      gearchiveerd: new FormControl(false, [Validators.required]),
    });

    this.route.params.subscribe(params => {
      this.discipline = params['discipline'];
      this.wedstrijdId = params['id'];

      if (this.wedstrijdId) {
        switch (this.discipline) {
          case 'jumping':
            this.getJumping(this.wedstrijdId);
            break;
          case 'dressuur':
            this.getDressage(this.wedstrijdId);
            break;
          case 'eventing':
            this.getEventing(this.wedstrijdId);
            break;
          case 'overig':
            this.getOverig(this.wedstrijdId);
            break;
          default:
            break;
        }
      }
    });
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.wedstrijd.updateBy(this.form.value);

    switch (this.discipline) {
      case 'jumping':
        this.wedstrijdService.saveJumping(this.wedstrijd);
        break;
      case 'dressuur':
        this.wedstrijdService.saveDressage(this.wedstrijd);
        break;
      case 'eventing':
        this.wedstrijdService.saveEventing(this.wedstrijd);
        break;
      case 'overig':
        this.wedstrijdService.saveOverig(this.wedstrijd);
        break;
      default:
        break;
    }
  }

  onCancel(event) {
    event.preventDefault();
    this.navigateBack();
  }

  onDelete(dressage, event) {
    event.preventDefault();
    switch (this.discipline) {
      case 'jumping':
        this.wedstrijdService.deleteJumping(this.wedstrijd.id);
        break;
      case 'dressuur':
        this.wedstrijdService.deleteDressage(this.wedstrijd.id);
        break;
      case 'eventing':
        this.wedstrijdService.deleteEventing(this.wedstrijd.id);
        break;
      case 'overig':
        this.wedstrijdService.deleteOverig(this.wedstrijd.id);
        break;
      default:
        break;
    }
  }

  navigateBack() {
    this.router.navigate([`/admin/wedstrijd/${this.discipline}`]);
  }

  getJumping(id) {
    this.wedstrijdService.getJumping(id).subscribe(jumping => {
      this.wedstrijd = jumping;
      if (!this.wedstrijd.id) {
        this.wedstrijd.id = id;
      }

      this.form.patchValue(this.wedstrijd);
    });
  }

  getDressage(id) {
    this.wedstrijdService.getDressage(id).subscribe(dressage => {
      this.wedstrijd = dressage;
      if (!this.wedstrijd.id) {
        this.wedstrijd.id = id;
      }

      this.form.patchValue(this.wedstrijd);
    });
  }

  getEventing(id) {
    this.wedstrijdService.getEventing(id).subscribe(eventing => {
      this.wedstrijd = eventing;
      if (!this.wedstrijd.id) {
        this.wedstrijd.id = id;
      }

      this.form.patchValue(this.wedstrijd);
    });
  }

  getOverig(id) {
    this.wedstrijdService.getOverig(id).subscribe(overig => {
      this.wedstrijd = overig;
      if (!this.wedstrijd.id) {
        this.wedstrijd.id = id;
      }

      this.form.patchValue(this.wedstrijd);
    });
  }
}
