import { Component, OnInit } from '@angular/core';
import { WedstrijdService } from '../../../services/wedstrijden.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-admin-wedstrijd',
  templateUrl: './wedstrijd.component.html',
})
export class AdminWedstrijdComponent implements OnInit {
  wedstrijden;
  allWedstrijden;
  discipline;
  searchForm;
  showGearchiveerd = false;
  order = 'sorteerDatum';
  reverse = false;
  deelVanBelgie: String = 'Vlaanderen';
  p: number = 1;

  constructor(
    private wedstrijdService: WedstrijdService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.discipline = params['discipline'];
      this.showGearchiveerd = false;
      switch (this.discipline) {
        case 'jumping':
          this.getJumpings();
          break;
        case 'dressuur':
          this.getDressages();
          break;
        case 'eventing':
          this.getEventings();
          break;
        case 'overig':
          this.getOverige();
          break;
        default:
          break;
      }
    });

    this.searchForm = new FormGroup({
      search: new FormControl(),
    });

    this.searchForm.valueChanges.subscribe(form => {
      this.wedstrijden = this.allWedstrijden.filter(item => {
        return (
          (item.locatie ? item.locatie.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.adres ? item.adres.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.gemeente ? item.gemeente.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.provincie ? item.provincie.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.organisatie ? item.organisatie.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.type ? item.type.toUpperCase().includes(form.search.toUpperCase()) : '')
        );
      });
    });
  }

  chanceOrder(col) {
    if (this.order === col) {
      this.reverse = !this.reverse;
    } else {
      this.reverse = false;
      this.order = col;
    }
  }

  archive() {
    this.wedstrijden.forEach(wedstrijd => {
      if (wedstrijd.einddatum) {
        if (new Date(wedstrijd.einddatum) < new Date()) {
          wedstrijd.gearchiveerd = true;
          this.wedstrijdService.saveWedstrijd(this.discipline, wedstrijd);
        }
      } else {
        if (new Date(wedstrijd.startdatum) < new Date()) {
          wedstrijd.gearchiveerd = true;
          this.wedstrijdService.saveWedstrijd(this.discipline, wedstrijd);
        }
      }
    });
  }

  changeGeactiveerd() {
    this.showGearchiveerd = !this.showGearchiveerd;
    switch (this.discipline) {
      case 'jumping':
        this.getJumpings();
        break;
      case 'dressuur':
        this.getDressages();
        break;
      case 'eventing':
        this.getEventings();
        break;
      case 'overig':
        this.getOverige();
        break;
      default:
        break;
    }
  }


  toDetails(id) {
    this.router.navigate([`/admin/wedstrijd/${this.discipline}/details/${id}`]);
  }

  onAdd() {
    this.router.navigate([`/admin/wedstrijd/${this.discipline}/details`]);
  }

  getJumpings() {
    this.wedstrijdService.getAllJumpings().subscribe(jumpings => {
      if (this.showGearchiveerd) {
        jumpings = jumpings.filter(jumping => {
          if (jumping.gearchiveerd) {
            return jumping.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        jumpings = jumpings.filter(jumping => {
          if (!jumping.gearchiveerd) {
            return jumping.gearchiveerd === this.showGearchiveerd;
          }
        });
      }
      this.wedstrijden = this.allWedstrijden = jumpings;
    });
  }

  getDressages() {
    this.wedstrijdService.getAllDressages().subscribe(dressages => {
      if (this.showGearchiveerd) {
        dressages = dressages.filter(dressage => {
          if (dressage.gearchiveerd) {
            return dressage.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        dressages = dressages.filter(dressage => {
          if (!dressage.gearchiveerd) {
            return dressage.gearchiveerd === this.showGearchiveerd;
          }
        });
      }
      this.wedstrijden = this.allWedstrijden = dressages;
    });
  }

  getEventings() {
    this.wedstrijdService.getAllEventings().subscribe(eventings => {
      if (this.showGearchiveerd) {
        eventings = eventings.filter(eventing => {
          if (eventing.gearchiveerd) {
            return eventing.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        eventings = eventings.filter(eventing => {
          if (!eventing.gearchiveerd) {
            return eventing.gearchiveerd === this.showGearchiveerd;
          }
        });
      }
      this.wedstrijden = this.allWedstrijden = eventings;
    });
  }

  getOverige() {
    this.wedstrijdService.getAllOverige().subscribe(overige => {
      if (this.showGearchiveerd) {
        overige = overige.filter(overig => {
          if (overig.gearchiveerd) {
            return overig.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        overige = overige.filter(overig => {
          if (!overig.gearchiveerd) {
            return overig.gearchiveerd === this.showGearchiveerd;
          }
        });
      }
      this.wedstrijden = this.allWedstrijden = overige;
    });
  }
}
