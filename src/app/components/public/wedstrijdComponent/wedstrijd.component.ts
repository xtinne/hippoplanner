import { Component, OnInit } from '@angular/core';
import { WedstrijdService } from '../../../services/wedstrijden.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-wedstrijd',
  templateUrl: './wedstrijd.component.html',
})
export class WedstrijdComponent implements OnInit {
  wedstrijden;
  allWedstrijden;
  discipline;
  deelVanBelgie;
  searchForm: FormGroup;
  showGearchiveerd = false;
  order = 'sorteerDatum';
  reverse = false;
  loading = true;
  p: number = 1;

  constructor(
    private wedstrijdService: WedstrijdService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.discipline = params['discipline'];
      this.deelVanBelgie = params['deelVanBelgie'];
      this.showGearchiveerd = false;
      switch (this.discipline) {
        case 'jumping':
          this.getJumpings(this.deelVanBelgie);
          break;
        case 'dressuur':
          this.getDressages(this.deelVanBelgie);
          break;
        case 'eventing':
          this.getEventings(this.deelVanBelgie);
          break;
        case 'overig':
          this.getOverige(this.deelVanBelgie);
          break;
        default:
          break;
      }
    });

    this.searchForm = new FormGroup({
      search: new FormControl(),
    });

    this.searchForm.valueChanges.subscribe(form => {
      this.wedstrijden = this.allWedstrijden.filter(item => {
        return (
          (item.locatie ? item.locatie.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.adres ? item.adres.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.gemeente ? item.gemeente.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.provincie ? item.provincie.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.organisatie ? item.organisatie.toUpperCase().includes(form.search.toUpperCase()) : '') ||
          (item.type ? item.type.toUpperCase().includes(form.search.toUpperCase()) : '')
        );
      });
    });
  }

  changeGeactiveerd() {
    this.showGearchiveerd = !this.showGearchiveerd;
    switch (this.discipline) {
      case 'jumping':
        this.getJumpings(this.deelVanBelgie);
        break;
      case 'dressuur':
        this.getDressages(this.deelVanBelgie);
        break;
      case 'eventing':
        this.getEventings(this.deelVanBelgie);
        break;
      case 'overig':
        this.getOverige(this.deelVanBelgie);
        break;
      default:
        break;
    }
  }

  toDetails(id) {
    this.router.navigate([`/wedstrijd/${this.discipline}/${this.deelVanBelgie}/${id}`]);
  }

  changeDeelVanBelgie(dvb) {
    this.router.navigate([`/wedstrijd/${this.discipline}/${dvb}`]);
  }

  chanceOrder(col) {
    if (this.order === col) {
      this.reverse = !this.reverse;
    } else {
      this.reverse = false;
      this.order = col;
    }
  }

  getJumpings(deelVanBelgie) {
    this.loading = true;
    this.wedstrijdService.getAllJumpings().subscribe(jumpings => {
      if (this.showGearchiveerd) {
        jumpings = jumpings.filter(jumping => {
          if (jumping.gearchiveerd) {
            return jumping.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        jumpings = jumpings.filter(jumping => {
          if (!jumping.gearchiveerd) {
            return jumping.gearchiveerd === this.showGearchiveerd;
          }
        });
      }

      if (deelVanBelgie === 'overzicht') {
        this.allWedstrijden = this.wedstrijden = jumpings;
      } else if (deelVanBelgie === 'vlaanderen' || deelVanBelgie === 'wallonie') {
        this.wedstrijden = this.allWedstrijden = jumpings.filter(jumping => {
          if (jumping.deelVanBelgie) {
            return jumping.deelVanBelgie.toLowerCase() === deelVanBelgie;
          }
        });
      }
      this.loading = false;
    });
  }

  getDressages(deelVanBelgie) {
    this.loading = true;
    this.wedstrijdService.getAllDressages().subscribe(dressages => {
      if (this.showGearchiveerd) {
        dressages = dressages.filter(dressage => {
          if (dressage.gearchiveerd) {
            return dressage.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        dressages = dressages.filter(dressage => {
          if (!dressage.gearchiveerd) {
            return dressage.gearchiveerd === this.showGearchiveerd;
          }
        });
      }

      if (deelVanBelgie === 'overzicht') {
        this.allWedstrijden = this.wedstrijden = dressages;
      } else if (deelVanBelgie === 'vlaanderen' || deelVanBelgie === 'wallonie') {
        this.wedstrijden = this.allWedstrijden = dressages.filter(dressage => {
          if (dressage.deelVanBelgie) {
            return dressage.deelVanBelgie.toLowerCase() === deelVanBelgie;
          }
        });
      }
      this.loading = false;
    });
  }

  getEventings(deelVanBelgie) {
    this.loading = true;
    this.wedstrijdService.getAllEventings().subscribe(eventings => {
      if (this.showGearchiveerd) {
        eventings = eventings.filter(eventing => {
          if (eventing.gearchiveerd) {
            return eventing.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        eventings = eventings.filter(eventing => {
          if (!eventing.gearchiveerd) {
            return eventing.gearchiveerd === this.showGearchiveerd;
          }
        });
      }

      if (deelVanBelgie === 'overzicht') {
        this.allWedstrijden = this.wedstrijden = eventings;
      } else if (deelVanBelgie === 'vlaanderen' || deelVanBelgie === 'wallonie') {
        this.wedstrijden = this.allWedstrijden = eventings.filter(eventing => {
          if (eventing.deelVanBelgie) {
            return eventing.deelVanBelgie.toLowerCase() === deelVanBelgie;
          }
        });
      }
      this.loading = false;
    });
  }

  getOverige(deelVanBelgie) {
    this.loading = true;
    this.wedstrijdService.getAllOverige().subscribe(overige => {
      if (this.showGearchiveerd) {
        overige = overige.filter(overig => {
          if (overig.gearchiveerd) {
            return overig.gearchiveerd === this.showGearchiveerd;
          }
        });
      } else {
        overige = overige.filter(overig => {
          if (!overig.gearchiveerd) {
            return overig.gearchiveerd === this.showGearchiveerd;
          }
        });
      }

      if (deelVanBelgie === 'overzicht') {
        this.allWedstrijden = this.wedstrijden = overige;
      } else if (deelVanBelgie === 'vlaanderen' || deelVanBelgie === 'wallonie') {
        this.wedstrijden = this.allWedstrijden = overige.filter(overig => {
          if (overig.deelVanBelgie) {
            return overig.deelVanBelgie.toLowerCase() === deelVanBelgie;
          }
        });
      }
      this.loading = false;
    });
  }
}
