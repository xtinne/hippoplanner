import { Component, OnInit } from '@angular/core';
import { WedstrijdService } from '../../../services/wedstrijden.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-wedstrijd-detail',
  templateUrl: './wedstrijdDetail.component.html',
})
export class WedstrijdDetailComponent implements OnInit {
  wedstrijdId;
  wedstrijd;
  discipline;
  constructor(private wedstrijdService: WedstrijdService, private route: ActivatedRoute, public sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.discipline = params['discipline'];
      this.wedstrijdId = params['id'];

      if (this.wedstrijdId) {
        switch (this.discipline) {
          case 'jumping':
            this.getJumping(this.wedstrijdId);
            break;
          case 'dressuur':
            this.getDressage(this.wedstrijdId);
            break;
          case 'eventing':
            this.getEventing(this.wedstrijdId);
            break;
          case 'overig':
            this.getOverig(this.wedstrijdId);
            break;
          default:
            break;
        }
      }
    });
  }

  getJumping(id) {
    this.wedstrijdService.getJumping(id).subscribe(jumping => {
      this.wedstrijd = jumping;
    });
  }

  getDressage(id) {
    this.wedstrijdService.getDressage(id).subscribe(dressage => {
      this.wedstrijd = dressage;
    });
  }

  getEventing(id) {
    this.wedstrijdService.getEventing(id).subscribe(eventing => {
      this.wedstrijd = eventing;
    });
  }

  getOverig(id) {
    this.wedstrijdService.getOverig(id).subscribe(overig => {
      this.wedstrijd = overig;
    });
  }
}
