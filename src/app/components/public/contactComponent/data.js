console.log(data.data.map(d => {
        console.log(d[6]);
      const startdatum = d[0].replace(/\\/g, '').split(' - ')[0]
      const einddatum = d[0].replace(/\\/g, '').split(' - ')[1]
      const inschrijvingLink = d[5].replace(/\\/g, '').match(/(?<=")(.*?)(?=")/);
      const inschrijvingText = d[5].replace(/\\/g, '').match(/(?<=>)(.*?)(?=<)/);
      const startLink = d[6].replace(/\\/g, '').match(/(?<=")(.*?)(?=")/);
      const startText = d[6].replace(/\\/g, '').match(/(?<=>)(.*?)(?=<)/);
      const object = {
          "startdatum": "2018-" + startdatum.replace(/\//g, '-').split('').reverse().join(''),
          "einddatum": d[0].includes('-') ? "2018-" + einddatum.replace(/\//g, '-').split('').reverse().join('') : '',
          "organisatie": d[1],
          "locatie": d[2],
          "gemeente": d[3],
          "provincie": d[4],
          "inschrijving": {
              "link": inschrijvingLink ? inschrijvingLink[0] : '',
              "text": inschrijvingText ? inschrijvingText[0] : '',
          },
          "startlijst": {
            "link": startLink ? startLink[0] : '',
            "text": startText ? startText[0] : '',
          },
          "gearchiveerd": false,
          "deelVanBelgie": "Vlaanderen"
      }
    //   this.wedstrijdService.saveOverig(object).subscribe(() => {
    //   });
      return object
    }));


    console.log(data.data.map(d => {
        const startdatum = d[0].replace(/\\/g, '').split(' - ')[0]
        const einddatum = d[0].replace(/\\/g, '').split(' - ')[1]
        const object = {
            "startdatum": "2018-" + startdatum.replace(/\//g, '-').split('').reverse().join(''),
            "einddatum": d[0].includes('-') ? "2018-" + einddatum.replace(/\//g, '-').split('').reverse().join('') : '',
            "type": d[1],
            "locatie": d[2],
            "gemeente": d[3],
            "provincie": d[4],
            "informatie": d[5],
            "gearchiveerd": false,
            "deelVanBelgie": "Wallonie"
        }
      //   this.wedstrijdService.saveOverig(object).subscribe(() => {
      //   });
        return object
      }));