import { Component, OnInit } from '@angular/core';
import { WedstrijdService } from '../../../services/wedstrijden.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
})
export class ContactComponent implements OnInit {
  name: string;
  mail: string;
  message: string;
  emailResponse: string;

  constructor(private wedstrijdService: WedstrijdService, private httpClient: HttpClient) { }

  ngOnInit() { }

  processForm() {
    const params = {
      from: this.mail,
      subject: `Contact ${this.name}`,
      title: `Contact ${this.name}`,
      preheader: `Contact ${this.name}`,
      content: `<p>${this.message}</p>`
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    this.httpClient.post('/mail', params, httpOptions).subscribe(
      data => {
        console.log(data)
        this.emailResponse = "Mail succesvol verzonden!"; 
      },
      error => {
        console.log(error)
        this.emailResponse =  "Er is iets mis gegaan. Probeer later opnieuw.";
      }
    )
  }
}
