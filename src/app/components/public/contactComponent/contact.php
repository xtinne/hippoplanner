<?php

$name = isset($_POST["name"]) ? $_POST["name"] : "";
$email = isset($_POST["email"]) ? $_POST["email"] : "";
$message = isset($_POST["message"]) ? $_POST["message"] : "";

if ($name == "" || $email == "" || $message == "") { 
    echo "Gelieve alle velden in te vullen.";
} else { 
    $EmailTo = "info@hippoplanner.be";
    $Subject = "Contact " . $name;
    $Body = $message;

    $success = mail($EmailTo, $Subject, $Body, "From:" . $email);

    if ($success){
        echo "Mail succesvol verzonden!"; 
    } else {
        echo "Er is iets mis gegaan. Probeer later opnieuw.";
    }
}

?>