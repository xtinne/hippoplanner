import {
  IWedstrijdDTO,
  IFotograaf,
  IInschrijving,
  IStartlijst,
} from './../services/wedstrijden.service';

export class Wedstrijd {
  id: string;
  startdatum: Date;
  einddatum?: Date;
  organisatie: string;
  locatie: string;
  adres: string;
  gemeente: string;
  provincie: string;
  deelVanBelgie: string;
  inschrijving?: IInschrijving;
  startlijst?: IStartlijst;
  resultaten?: string;
  proeven?: string[];
  bodem?: string;
  fotograaf?: IFotograaf[];
  gearchiveerd: boolean;
  kaart?: string;

  constructor(data?: IWedstrijdDTO) {
    if (data) {
      Object.assign(this, data);
    }
  }

  updateBy(data) {
    Object.assign(this, data);
  }
}
