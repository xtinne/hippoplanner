// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD35UadpdR2xAOf9T77xtZgpPlLivPPGY0",
    authDomain: "hippoplanner-4e6d6.firebaseapp.com",
    databaseURL: "https://hippoplanner-4e6d6.firebaseio.com",
    projectId: "hippoplanner-4e6d6",
    storageBucket: "hippoplanner-4e6d6.appspot.com",
    messagingSenderId: "484538562856"
  }
};
